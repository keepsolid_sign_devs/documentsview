#ifndef ABSTRACTITEMMODEL_H
#define ABSTRACTITEMMODEL_H

#include <QAbstractItemModel>
#include <QTimer>
#include <QRectF>
#include <QPointF>
#include <memory>

class Document
{

public:
    Document(QString documentName, QString documentAuthor, QString documentState);

    QString getDocumentName() const;
    QString getDocumentAuthor() const;
    QString getDocumentState() const;

    void setDocumentName(QString newDocumentName);
private:

    QString documentName;
    QString documentAuthor;
    QString documentState;
};

class AbstractItemModel : public QAbstractItemModel
{
    Q_OBJECT
public:

    enum DocumentRoles {

            DocumentNameTypeRole = Qt::UserRole + 1,
            DocumentAuthorTypeRole,
            DocumentStateTypeRole,
            DocumentSelectedTypeRole
        };

    enum KSDocumentsFilterState: int {

        AllDocuments = 0,
        WaitingForMe,
        WaitingForOthers
    };
    Q_PROPERTY(KSDocumentsFilterState filterState MEMBER _filterState NOTIFY filterStateChanged)
    Q_ENUM(KSDocumentsFilterState);

    AbstractItemModel(QObject *parent = 0);

    Q_INVOKABLE void addDocument();
    Q_INVOKABLE void selectDocument(int index);
    Q_INVOKABLE void deselectDocument(int index);
    Q_INVOKABLE bool isDocumentSelected(int index);
    Q_INVOKABLE bool isRectsIntersected(QRectF rect1, QRectF rect2);
    Q_INVOKABLE bool isRectContainsPoint(QRectF rect, QPointF point);
    Q_INVOKABLE void deselectAllDocuments();
    Q_INVOKABLE void selectDocumentWithShift(int index);
    Q_INVOKABLE void controlPressed(bool pressed);
    Q_INVOKABLE bool isControlPressed();
    Q_INVOKABLE void shiftPressed(bool pressed);
    Q_INVOKABLE bool isShiftPressed();

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;

    QModelIndex parent(const QModelIndex &child) const;

    int columnCount(const QModelIndex &parent = QModelIndex()) const;

protected:

    QHash<int, QByteArray> roleNames() const;
signals:
    void isControlPressedChanged();
    void isShiftPressedChanged();
    void filterStateChanged();
private slots:
    void removeDocument();
private:

    QList<std::shared_ptr<Document>> _documents;
    QList<std::shared_ptr<Document>> _selectedDocuments;
    int _lastSelectedIndex = -1;
    bool _isControlPressed = false;
    bool _isShiftPressed = false;
    KSDocumentsFilterState _filterState = AllDocuments;
};

#endif // ABSTRACTITEMMODEL_H
