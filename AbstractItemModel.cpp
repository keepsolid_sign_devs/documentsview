#include "AbstractItemModel.h"
#include <QTimer>
#include <QRectF>
#include <memory.h>
#include <math.h>
// Document

Document::Document(QString const documentName, QString const documentAuthor, QString const documentState) :
    documentName(documentName), documentAuthor(documentAuthor), documentState(documentState)
{

}

QString Document::getDocumentName() const {

    return documentName;
}

QString Document::getDocumentAuthor() const {

    return documentAuthor;
}

QString Document::getDocumentState() const {

    return documentState;
}

void Document::setDocumentName(QString newDocumentName) {

    documentName = newDocumentName;
}

// Abstract model

AbstractItemModel::AbstractItemModel(QObject *parent):
    QAbstractItemModel(parent), _documents(), _selectedDocuments()
{

}

void AbstractItemModel::addDocument() {

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    QString documentName = QString("Document %1").arg(QString::number(_documents.size()));
    _documents.append(std::make_shared<Document>(documentName, "Document Author", "Draft"));
    endInsertRows();

    QModelIndex index = createIndex(_documents.size(), 0, static_cast<void *>(0));
    emit dataChanged(index, index);
}

int AbstractItemModel::rowCount(const QModelIndex & parent) const {

    if (parent.isValid()) {

        return 0;
    }
    return _documents.size();
}

QVariant AbstractItemModel::data(const QModelIndex & index, int role) const {

    if (index.row() < 0 || index.row() >= _documents.size())
        return QVariant();

    std::shared_ptr<Document> document = _documents[index.row()];
    switch (role) {
    case DocumentNameTypeRole:

        return document->getDocumentName();
        break;
    case DocumentAuthorTypeRole:

        return document->getDocumentAuthor();
        break;
    case DocumentStateTypeRole:

        return document->getDocumentState();
        break;
    case DocumentSelectedTypeRole:

        bool isSelected = false;
        if (!_selectedDocuments.isEmpty() && _selectedDocuments.contains(document)) {

            isSelected = true;
        }
        return isSelected;
        break;
    }
    return QVariant();
}

QHash<int, QByteArray> AbstractItemModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[DocumentNameTypeRole] = "name";
    roles[DocumentAuthorTypeRole] = "author";
    roles[DocumentStateTypeRole] = "state";
    roles[DocumentSelectedTypeRole] = "selected";
    return roles;
}

QModelIndex AbstractItemModel::index(int row, int column,
                  const QModelIndex &parent) const {

    Q_UNUSED(parent);
    return createIndex(row, column, static_cast<void *>(0));
}

QModelIndex AbstractItemModel::parent(const QModelIndex &child) const {

    Q_UNUSED(child);
    return QModelIndex();
}

int AbstractItemModel::columnCount(const QModelIndex &parent) const {

    Q_UNUSED(parent);
    return 1;
}

void AbstractItemModel::removeDocument() {

    if (!_documents.isEmpty()) {

        beginRemoveRows(QModelIndex(), 0, 0);
        _documents.removeFirst();
        endRemoveRows();
        QModelIndex index = createIndex(0, 0, static_cast<void *>(0));
        emit dataChanged(index, index);
    }
}

void AbstractItemModel::selectDocument(int index) {

    QModelIndex modelIndex = createIndex(index, index, static_cast<void *>(0));
    std::shared_ptr<Document> document = _documents[index];
    _selectedDocuments.append(document);
    _lastSelectedIndex = index;
    emit dataChanged(modelIndex, modelIndex);
}

void AbstractItemModel::selectDocumentWithShift(int index) {

    if (_selectedDocuments.size() > 0) {

        this->deselectAllDocuments();
        int startIndex = std::min(_lastSelectedIndex, index);
        int endIndex = std::max(_lastSelectedIndex, index);

        for (int i = startIndex; i <= endIndex; i++) {

            if (!this->isDocumentSelected(i)) {

                std::shared_ptr<Document> document = _documents[i];
                _selectedDocuments.append(document);
            }
        }

        QModelIndex startModelIndex = createIndex(startIndex, 0, static_cast<void *>(0));
        QModelIndex endModelIndex = createIndex(endIndex, 0, static_cast<void *>(0));
        emit dataChanged(startModelIndex, endModelIndex);
    } else {

        this->selectDocument(index);
    }
}

bool AbstractItemModel::isRectsIntersected(QRectF rect1, QRectF rect2) {

    return rect1.intersects(rect2);
}

void AbstractItemModel::deselectDocument(int index) {

    QModelIndex modelIndex = createIndex(index, index, static_cast<void *>(0));
    std::shared_ptr<Document> document = _documents[index];
    _selectedDocuments.removeOne(document);
    emit dataChanged(modelIndex, modelIndex);
}

bool AbstractItemModel::isDocumentSelected(int index) {

    std::shared_ptr<Document> document = _documents[index];
    bool isDocumentSelected = false;
    if (_selectedDocuments.contains(document)) {

        isDocumentSelected = true;
    }
    return isDocumentSelected;
}

bool AbstractItemModel::isRectContainsPoint(QRectF rect, QPointF point) {

    bool isRectContainsPoint = rect.contains(point);
    return isRectContainsPoint;
}

void AbstractItemModel::deselectAllDocuments() {

    _selectedDocuments.clear();
    QModelIndex startIndex = createIndex(0, 0, static_cast<void*>(0));
    QModelIndex endIndex = createIndex(_documents.size() - 1, 0, static_cast<void*>(0));
    emit dataChanged(startIndex, endIndex);
}

void AbstractItemModel::controlPressed(bool pressed) {

    _isControlPressed = pressed;
}

bool AbstractItemModel::isControlPressed() {

    return _isControlPressed;
}

void AbstractItemModel::shiftPressed(bool pressed) {

    _isShiftPressed = pressed;
}

bool AbstractItemModel::isShiftPressed() {

    return _isShiftPressed;
}
