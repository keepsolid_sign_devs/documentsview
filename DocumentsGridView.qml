import QtQuick 2.0
import QtQuick.Controls 1.4
import AbstractItemModel 1.0

Item {

    id: documentsGridView
    focus: true
    Keys.onPressed: {

        if (event.key == Qt.Key_Control) {

            abstractItemModel.controlPressed(true);
        }
        if (event.key == Qt.Key_Shift) {

            abstractItemModel.shiftPressed(true);
        }
    }

    Keys.onReleased: {

        if (event.key == Qt.Key_Control) {

            abstractItemModel.controlPressed(false);
        }
        if (event.key == Qt.Key_Shift) {

            abstractItemModel.shiftPressed(false);
        }
    }

    Button {
        id: addDocumentButton
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        anchors.top: parent.top
        anchors.topMargin: 5
        text: qsTr("Add document")
        onClicked: abstractItemModel.addDocument()
    }

    GridView {

        id: gridView
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        anchors.top: addDocumentButton.bottom
        anchors.topMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        flickableDirection: Flickable.AutoFlickDirection
        cellWidth: {

            var cellWidth = 160;
            return cellWidth;
        }
        cellHeight: {

            var heightCoef = 1.625;
            var cellHeight = cellWidth * heightCoef;
            return cellHeight;
        }

        clip: true
        highlightRangeMode: GridView.ApplyRange
//        Behavior on cellWidth { NumberAnimation { duration: 50; } }
//        Behavior on cellHeight { NumberAnimation { duration: 50; } }

        model: abstractItemModel

        delegate: Cell{

            id: cell
            documentNameAlias: model.name
            documentAuthorAlias: model.author
            documentStateAlias: model.state
            documentSelectedAlias: model.selected
            height: gridView.cellHeight
            width: gridView.cellWidth
        }

//        onWidthChanged: {

//            var cellSize = calculatedCellSize();
//            gridView.cellWidth = cellSize.width;
//            gridView.cellHeight = cellSize.height;
//        }

//        function calculatedCellSize() {

//            var minimumCellWidth = 160;
//            var actualCellWidth = minimumCellWidth;
//            var heightCoef = 1.625;
//            var minimumCellHeight = minimumCellWidth * heightCoef;
//            var actualCellHeight = minimumCellHeight;
//            var gridViewWidth = gridView.width;
//            var cellCount = Math.floor(gridViewWidth / actualCellWidth);
//            if (gridViewWidth > minimumCellWidth) {

//                while (actualCellWidth * cellCount <= gridViewWidth) {

//                    actualCellWidth++;
//                }
//                while (actualCellWidth * cellCount > gridViewWidth) {

//                    actualCellWidth--;
//                }
//                actualCellHeight = actualCellWidth * heightCoef;
//            }
//            return Qt.size(actualCellWidth, actualCellHeight)
//        }

        MouseArea {

            id: selectMouseArea
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton
            hoverEnabled: selectMouseArea.pressed
            preventStealing: true
            propagateComposedEvents: true
            onPositionChanged: {

                if (selectMouseArea.pressed) {

                    if (!selectLayer.selectionStarted) {

                        startSelectingDocuments();
                    }

                    if(mouseX >= selectLayer.newX) {
                        selectLayer.width = (mouseX + 10) < (selectMouseArea.x + selectMouseArea.width) ? (mouseX - selectLayer.x) : selectLayer.width;
                    } else {
                        selectLayer.x = mouseX < selectMouseArea.x ? selectMouseArea.x : mouseX;
                        selectLayer.width = selectLayer.newX - selectLayer.x;
                    }
                    if(mouseY >= selectLayer.newY) {
                        selectLayer.height = (mouseY + 10) < (selectMouseArea.y + selectMouseArea.height) ? (mouseY - selectLayer.y) : selectLayer.height;
                    } else {
                        selectLayer.y = mouseY < selectMouseArea.y ? selectMouseArea.y : mouseY;
                        selectLayer.height = selectLayer.newY - selectLayer.y;
                    }
                    var selectLayerFrame = selectLayer.parent.mapToItem(null,
                                                                        selectLayer.x,
                                                                        selectLayer.y,
                                                                        selectLayer.width,
                                                                        selectLayer.height);
                    for(var child in gridView.contentItem.children) {

                        var childItem = gridView.contentItem.children[child];
                        if (childItem.objectName == "GridViewCell") {

                            var childFrame = childItem.contentRectangle.parent.mapToItem(null,
                                                                                         childItem.contentRectangle.x,
                                                                                         childItem.contentRectangle.y,
                                                                                         childItem.contentRectangle.width,
                                                                                         childItem.contentRectangle.height);
                            var isIntersected = abstractItemModel.isRectsIntersected(selectLayerFrame, childFrame);
                            if (isIntersected) {

                                if (!abstractItemModel.isDocumentSelected(childItem.cellIndex)) {

                                    abstractItemModel.selectDocument(childItem.cellIndex);
                                }
                            } else {

                                if (!isModificatorKeyPressed()) {

                                    if (abstractItemModel.isDocumentSelected(childItem.cellIndex)) {

                                        abstractItemModel.deselectDocument(childItem.cellIndex);
                                    }
                                }
                            }
                        }
                    }
                }
            }
//onClicked called in purpose of event interception(cell's mouse area shouldnt react on grag selection)
            onClicked: {

                if (selectLayer.selectionStarted) {

                    endSelectingDocuments();
                    mouse.accepted = true;
                } else {

                    if (isCellExistAtPoint(mouseX, mouseY)) {

                        mouse.accepted = false;
                    } else {

                        if (!isModificatorKeyPressed()) {

                            abstractItemModel.deselectAllDocuments();
                        }
                        mouse.accepted = true;
                    }
                }
            }

            onReleased: {

                var point = Qt.point(mouseX, mouseY);
                if (!selectMouseArea.contains(point)) {

                    endSelectingDocuments();
                    mouse.accepted = true;
                }
            }

            Rectangle {

                id: selectLayer
                property bool selectionStarted: false
                property int newX: 0
                property int newY: 0
                height: 0
                width: 0
                x: 0
                y: 0
                visible: false
                color: systemActivePalette.highlight
                opacity: 0.4
                radius: 2

                SystemPalette { id: systemActivePalette; colorGroup: SystemPalette.Inactive }
            }

            function startSelectingDocuments() {

                selectLayer.selectionStarted = true;
                selectLayer.visible = true;
                selectLayer.x = mouseX;
                selectLayer.y = mouseY;
                selectLayer.newX = mouseX;
                selectLayer.newY = mouseY;
                selectLayer.width = 0
                selectLayer.height = 0;
            }

            function endSelectingDocuments() {

                selectLayer.selectionStarted = false;
                selectLayer.visible = false;
                selectLayer.x = 0;
                selectLayer.y = 0;
                selectLayer.newX = 0;
                selectLayer.newY = 0;
                selectLayer.width = 0;
                selectLayer.height = 0;
            }

            function isCellExistAtPoint(x, y) {

                var isClickInCell = false;
                var inRootPoint = selectLayer.parent.mapToItem(null,
                                                               x,
                                                               y);
                for(var child in gridView.contentItem.children) {

                    var childItem = gridView.contentItem.children[child];
                    if (childItem.objectName == "GridViewCell") {

                        var childFrame = childItem.contentRectangle.parent.mapToItem(null,
                                                                                     childItem.contentRectangle.x,
                                                                                     childItem.contentRectangle.y,
                                                                                     childItem.contentRectangle.width,
                                                                                     childItem.contentRectangle.height);
                        if (abstractItemModel.isRectContainsPoint(childFrame, inRootPoint)) {

                            isClickInCell = true;
                            break;
                        }
                    }
                }
                return isClickInCell;
            }

            function isModificatorKeyPressed() {

                if (abstractItemModel.isControlPressed() || abstractItemModel.isShiftPressed()) {

                    return true;
                }
                return false;
            }
        }
    }
    ScrollBar {

            flickable: gridView
        }
}
