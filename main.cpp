#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <AbstractItemModel.h>
#include <QQuickView>
#include <qqmlcontext.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qmlRegisterType<AbstractItemModel>("AbstractItemModel", 1, 0, "AbstractItemModel");
    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

//    AbstractItemModel model;
//    engine.rootContext()->setContextProperty("AbstractItemModel", &model);

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
