import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import AbstractItemModel 1.0

ApplicationWindow {

    id: mainWindow
    visible: true
    width: 660
    height: 480
    title: qsTr("Hello World")

    property color selectedColor: "#2370b3"

    KSDocumentsFilterView {

        id: filtersView
    }

    DocumentsGridView {

        anchors {

            top: filtersView.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
    }

    AbstractItemModel {

        id: abstractItemModel
    }
}
