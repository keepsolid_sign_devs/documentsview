import QtQuick 2.0

Item {

    id: scrollbar;
    width: 10
    visible: (flickable.visibleArea.heightRatio < 1.0);
    anchors {
        top: flickable.top;
        right: flickable.right;
        bottom: flickable.bottom;
    }
    onVisibleChanged: {

        if (visible) {

            updateTimerState();
        }
    }

    property Flickable flickable: null;
    property int handleSize: 20;

    function updateTimerState() {

        if (clicker.containsMouse) {

            timer.stop();
        } else {

            timer.restart();
        }
    }

    function scrollDown () {
        flickable.contentY = Math.min (flickable.contentY + (flickable.height / 4), flickable.contentHeight - flickable.height);
    }
    function scrollUp () {
        flickable.contentY = Math.max (flickable.contentY - (flickable.height / 4), 0);
    }

    Timer {

        id: timer
        interval: 1000
        repeat: false
        running: false
        onTriggered: {

            backScrollbar.opacity = 0;
        }
    }

    Binding {
        target: handle;
        property: "y";
        value: (flickable.contentY * clicker.drag.maximumY / (flickable.contentHeight - flickable.height));
        when: (!clicker.drag.active);

    }
    Binding {
        target: flickable;
        property: "contentY";
        value: (handle.y * (flickable.contentHeight - flickable.height) / clicker.drag.maximumY);
        when: (clicker.drag.active || clicker.pressed);
    }

    Rectangle {
        id: backScrollbar;
        color: "#eaeaea";
        anchors { fill: parent; }
        Behavior on opacity {
            NumberAnimation {

                duration: 500;
            } }

        MouseArea {
            anchors.fill: parent;
            onClicked: { }
        }
        Item {
            id: groove;
            clip: true;
            anchors {
                fill: parent;
            }

            MouseArea {
                id: clicker;
                hoverEnabled: true
                drag {
                    target: handle;
                    minimumY: 0;
                    maximumY: (groove.height - handle.height);
                    axis: Drag.YAxis;
                }
                onContainsMouseChanged: {

                    updateTimerState();
                    if (clicker.containsMouse) {

                        backScrollbar.opacity = 1;
                    }
                }

                anchors { fill: parent; }
                onClicked: {
                    console.log("Mouse area onClicked")
                    flickable.contentY = (mouse.y / groove.height * (flickable.contentHeight - flickable.height));
                }
            }

            Item {
                id: handle;
                height: Math.max (20, (flickable.visibleArea.heightRatio * groove.height));
                anchors {
                    left: parent.left;
                    right: parent.right;
                }

                Rectangle {
                    id: backHandle;
                    color: "#2980cc";
                    anchors { fill: parent; }
                }
                onYChanged: {

                    backScrollbar.opacity = 1;
                    updateTimerState();
                }
            }
        }
    }
}
