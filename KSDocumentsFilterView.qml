import QtQuick 2.7
import AbstractItemModel 1.0

Row {

    id: filtersRow
    spacing: 5
    padding: 10

    property color selectedFilterColor: "#2980cc"
    property color defaultFilterColor: "#00000000"
    property color selectedFilterTextColor: "#ffffff"
    property color defaultFilterTextColor: "#2980cc"

    Rectangle {

        height: 30
        width: allDocumentsText.contentWidth + 30//paddingInDesign
        color: abstractItemModel.filterState == AbstractItemModel.AllDocuments ? selectedFilterColor : defaultFilterColor

        Text {

            id: allDocumentsText
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("All documents")
            font.family: "OpenSans"
            font.pointSize: 13
            color: abstractItemModel.filterState == AbstractItemModel.AllDocuments ? selectedFilterTextColor : defaultFilterTextColor
        }
        MouseArea {

            anchors.fill: parent
            onClicked: {

                abstractItemModel.filterState = AbstractItemModel.AllDocuments;
            }
        }
    }
    Rectangle {

        height: 30
        width: waitingForMeText.contentWidth + 30//paddingInDesign
        color: abstractItemModel.filterState == AbstractItemModel.WaitingForMe ? selectedFilterColor : defaultFilterColor

        Text {

            id: waitingForMeText
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Waiting for me")
            font.family: "OpenSans"
            font.pointSize: 13
            color: abstractItemModel.filterState == AbstractItemModel.WaitingForMe ? selectedFilterTextColor : defaultFilterTextColor
        }
        MouseArea {

            anchors.fill: parent
            onClicked: {

                abstractItemModel.filterState = AbstractItemModel.WaitingForMe;
            }
        }
    }
    Rectangle {

        height: 30
        width: waitingForOthersText.contentWidth + 30//paddingInDesign
        color: abstractItemModel.filterState == AbstractItemModel.WaitingForOthers ? selectedFilterColor : defaultFilterColor

        Text {

            id: waitingForOthersText
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Waiting for others")
            font.family: "OpenSans"
            font.pointSize: 13
            color: abstractItemModel.filterState == AbstractItemModel.WaitingForOthers ? selectedFilterTextColor : defaultFilterTextColor
        }
        MouseArea {

            anchors.fill: parent
            onClicked: {

                abstractItemModel.filterState = AbstractItemModel.WaitingForOthers;
            }
        }
    }
}
