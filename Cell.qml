import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

Item {

    id: cell
    objectName: "GridViewCell"
    property alias documentNameAlias: cell.documentNameStr
    property alias documentAuthorAlias: cell.documentAuthorStr
    property alias documentStateAlias: cell.documentStateStr
    property alias documentSelectedAlias: cell.documentSelected
    property alias cellMenu: menuFile
    property string documentNameStr
    property string documentAuthorStr
    property string documentStateStr
    property bool documentSelected
    property int cellIndex: index
    property Rectangle contentRectangle: baseRectangle

    Rectangle {

        id: spaceRectangle
        anchors.fill: parent

        Rectangle {

            id: baseRectangle
            anchors.fill: parent
            anchors.margins: 15
            clip: true
            color: "#f4f4f4"

            RectangularGlow {

                id: effect
                anchors.fill: hoveredRect
                glowRadius: 6
                color: "#19000000"
                cornerRadius: parent.radius
                visible: hoveredRect.visible
            }

            Rectangle {

                id: hoveredRect
                anchors.fill: parent
                color: "#f6f8fa"
                border.color: "#7f2980cc"
                border.width: 1
                visible: cellMouseArea.containsMouse
            }

            Rectangle {

                id: selectedView
                anchors.fill: parent
                color: "#daebf9"
                visible: documentSelected
                border.color: "#322980cc"
                border.width: 1
            }

            Menu {
                id: menuFile
                x: cellMouseArea.mouseX
                y: cellMouseArea.mouseY

                MenuItem {
                    text: qsTr("Open")
                    onTriggered: messageDialog.show(qsTr("Open action triggered"));
                }
                MenuItem {
                    text: qsTr("Copy")

                    onTriggered: Qt.quit();
                }
                MenuItem {
                    text: qsTr("Export")
                    onTriggered: Qt.quit();
                }
                MenuItem {
                    text: qsTr("Delete")
                    onTriggered: Qt.quit();
                }
                MenuItem {
                    text: qsTr("Arhive")
                    onTriggered: Qt.quit();
                }
                MenuItem {
                    text: qsTr("Save as Template")
                    onTriggered: Qt.quit();
                }
            }

            Image {
                id: image
                sourceSize.width: 0
                fillMode: Image.Stretch
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.top: parent.top
                anchors.topMargin: 10
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 70
                source: "11.jpg"
            }

            Image {
                id: stateImage
                width: 40
                height: 40
                anchors.top: image.top
                anchors.right: image.right
                source: "11.jpg"
            }

            Text {
                //main
                id: documentName
                text: documentNameStr
                //size
                height: 17
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.bottom: documentAuthor.top
                anchors.bottomMargin: 2
                //font
                font.pointSize: 10
                font.weight: Font.DemiBold
                font.family: "OpenSans"
                //other
                color: "#444444"
                verticalAlignment: Text.AlignVCenter
                clip: true
            }

            Text {
                //main
                id: documentAuthor
                text: documentAuthorStr
                //size
                height: 14
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.bottom: documentState.top
                anchors.bottomMargin: 3
                //font
                font.pointSize: 10
                font.weight: Font.Normal
                font.family: "OpenSans"
                //other
                color: "#8d8d8d"
                verticalAlignment: Text.AlignVCenter
                clip: true
            }

            Text {
                //main
                id: documentState
                text: documentStateStr
                //size
                height: 14
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 12
                //font
                font.pointSize: 10
                font.weight: Font.DemiBold
                font.family: "OpenSans"
                //other
                color: "#757575"
                verticalAlignment: Text.AlignVCenter
                clip: true
            }

            MouseArea {

                id: cellMouseArea
                anchors.fill: parent
                property bool isSelectionInAction: false
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                hoverEnabled: true

                onClicked: {

                    if (mouse.button == Qt.LeftButton) {

                        var isControlPressed = abstractItemModel.isControlPressed();
                        var isShiftPressed = abstractItemModel.isShiftPressed();

                        if (!isShiftPressed) {

                            if (!isControlPressed) {

                                abstractItemModel.deselectAllDocuments();
                            }

                            if (abstractItemModel.isDocumentSelected(index)) {


                                abstractItemModel.deselectDocument(index);
                            } else {

                                abstractItemModel.selectDocument(index);
                            }
                        } else {

                            abstractItemModel.selectDocumentWithShift(index);
                        }
                    } else if (mouse.button == Qt.RightButton) {

                        if (!abstractItemModel.isDocumentSelected(index)) {

                            abstractItemModel.deselectAllDocuments();
                            abstractItemModel.selectDocument(index);
                        }
                        menuFile.open()
                    }
                }
                onDoubleClicked: {

                    abstractItemModel.shiftPressed(false);
                    abstractItemModel.controlPressed(false);
                    abstractItemModel.deselectAllDocuments();
                    abstractItemModel.selectDocumentWithShift(index);
                    var component = Qt.createComponent("qrc:/DocumentView.qml")
                    var window = component.createObject(mainWindow)
                    window.show()
                }
            }
        }
    }
}
